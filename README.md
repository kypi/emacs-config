# `Emacs`-Configuration
*****

## **Content**

- **Compayn mode**
- **Flycheck mode**
- **Lua mode**
- **Merlin**
- **Tuareg**
- **Utop**
- **Markdown mode**
_ **YAML mode**